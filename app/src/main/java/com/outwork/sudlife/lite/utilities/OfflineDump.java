package com.outwork.sudlife.lite.utilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shravanch on 18-09-2018.
 */

public class OfflineDump {


    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getInputData() {
        return InputData;
    }

    public void setInputData(String inputData) {
        InputData = inputData;
    }

    public int getLocalId() {
        return LocalId;
    }

    public void setLocalId(int localId) {
        LocalId = localId;
    }

    public String getLocalDate() {
        return LocalDate;
    }

    public void setLocalDate(String localDate) {
        LocalDate = localDate;
    }

    @SerializedName("Type")
    @Expose
    private String Type;


    @SerializedName("InputData")
    @Expose
    private String InputData;

    @SerializedName("LocalId")
    @Expose
    private int LocalId;

    @SerializedName("LocalDate")
    @Expose
    private String LocalDate;



}

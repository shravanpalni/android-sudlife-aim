package com.outwork.sudlife.lite.planner.horizontalpicker;

public interface HorizontalPickerListener {
    void onStopDraggingPicker();
    void onDraggingPicker();
    void onDateSelected(Day item);
}
package com.outwork.sudlife.lite.dto.geocode;

/**
 * Created by Panch on 2/17/2017.
 */

public enum STATUS {
    OK, ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED, INVALID_REQUEST
}

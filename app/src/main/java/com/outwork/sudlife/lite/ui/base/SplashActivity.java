package com.outwork.sudlife.lite.ui.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.outwork.sudlife.lite.R;
import com.outwork.sudlife.lite.branches.BranchesMgr;
import com.outwork.sudlife.lite.branches.services.ContactsIntentService;
import com.outwork.sudlife.lite.dto.DeviceInfo;
import com.outwork.sudlife.lite.dto.UserObjectDto;
import com.outwork.sudlife.lite.lead.activities.ViewActivityLeadActivity;
import com.outwork.sudlife.lite.lead.db.ProposalCodesDao;
import com.outwork.sudlife.lite.lead.model.ProposalCodesModel;
import com.outwork.sudlife.lite.lead.services.LeadIntentService;
import com.outwork.sudlife.lite.localbase.ProductDataMgr;
import com.outwork.sudlife.lite.localbase.ProductMasterDto;
import com.outwork.sudlife.lite.localbase.StaticDataDto;
import com.outwork.sudlife.lite.localbase.StaticDataMgr;
import com.outwork.sudlife.lite.opportunity.OpportunityMgr;
import com.outwork.sudlife.lite.planner.SupervisorMgr;
import com.outwork.sudlife.lite.planner.activities.CreatePlanActivity;
import com.outwork.sudlife.lite.planner.models.SupervisorModel;
import com.outwork.sudlife.lite.planner.service.PlannerIntentService;
import com.outwork.sudlife.lite.planner.service.SupervisorIntentService;
import com.outwork.sudlife.lite.localbase.services.ProductListDownloadService;
import com.outwork.sudlife.lite.opportunity.services.OpportunityIntentService;
import com.outwork.sudlife.lite.restinterfaces.RestResponse;
import com.outwork.sudlife.lite.restinterfaces.RestService;
import com.outwork.sudlife.lite.restinterfaces.UserRestInterface;
import com.outwork.sudlife.lite.ui.BaseActivity;
import com.outwork.sudlife.lite.planner.activities.CalenderActivity;
import com.outwork.sudlife.lite.ui.activities.SignInActvity;
import com.outwork.sudlife.lite.utilities.Constants;
import com.outwork.sudlife.lite.IvokoApplication;
import com.outwork.sudlife.lite.utilities.SharedPreferenceManager;
import com.outwork.sudlife.lite.utilities.TimeUtils;
import com.outwork.sudlife.lite.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.outwork.sudlife.lite.restinterfaces.RestService.API_BASE_URL;

public class SplashActivity extends BaseActivity {
    public static final String TAG = SplashActivity.class.getSimpleName();

    private IvokoApplication application;
    private static int SPLASH_TIME_OUT = 3000;
    private UserObjectDto userObjectDto;
    //private MixpanelAPI mixpanel;
    private DeviceInfo deviceInfo;
    private String apprelease = "";
    private String appversion = "";
    private String libversion = "";
    private String os = "";
    private String osversion = "";
    private String model = "";
    private String manufacturer = "";
    private String currentVersion = null;
    private String playstoreVersion = null;
    private String isMandate = null;
    private Dialog updatedialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        application = (IvokoApplication) getApplication();
        String projectToken = application.getApplicationContext().getString(R.string.mixpaneltoken);
        //mixpanel = MixpanelAPI.getInstance(SplashActivity.this, projectToken);
        userObjectDto = new UserObjectDto(SplashActivity.this);
        deviceInfo = Utils.getInfosAboutDevice(SplashActivity.this, 1);
        apprelease = deviceInfo.getApprelease();
        appversion = deviceInfo.getAppversioncode();
        libversion = deviceInfo.getLibversion();
        os = deviceInfo.getOs();
        osversion = deviceInfo.getOsversion();
        model = deviceInfo.getModel();
        manufacturer = deviceInfo.getManufacturer();
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //Log.i("Splash","Playstore version = = = ="+currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (isNetworkAvailable()) {


            if (!(API_BASE_URL.equalsIgnoreCase("https://sudlifeservices.outwork.in:443/"))) {// do not call for dev server

                getVersionHistory(currentVersion, "android");

            }

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                        initServices();
                        Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                        startActivity(in);
                        finish();
                    } else {
                        Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                        startActivity(in);
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void initServices() {
        if (isNetworkAvailable()) {


            List<String> stagesList = OpportunityMgr.getInstance(SplashActivity.this).getOpportunitystagesnamesList(userid);
            if (stagesList.isEmpty()) {
                Log.i("shravan", "stagesList empty");
                LeadIntentService.insertProposalCodes(SplashActivity.this);

            }
            List<ProposalCodesModel> proposalCodesModelList = ProposalCodesDao.getInstance(SplashActivity.this).getProposalCodesList(groupId);
            if (proposalCodesModelList.isEmpty()) {
                Log.i("shravan", "proposalCodesModelList empty");
                OpportunityIntentService.insertOpportunityStages(SplashActivity.this);
            }
            List<ProductMasterDto> prodList = new ProductDataMgr(SplashActivity.this).getProductList(groupId);
            if (prodList.isEmpty()) {
                Log.i("shravan", "prodList empty");
                ProductListDownloadService.getProductListinDB(SplashActivity.this);

            }
            List<StaticDataDto> staticList = new StaticDataMgr(SplashActivity.this).getAllStaticData(groupId);
            if (staticList.isEmpty()) {
                Log.i("shravan", "staticList empty");
                ProductListDownloadService.getMasterListinDB(SplashActivity.this);

            }
            List<String> customerList = BranchesMgr.getInstance(SplashActivity.this).getCustomerNamesList(userid);
            if (customerList.isEmpty()) {
                Log.i("shravan", "customerList empty");
                ContactsIntentService.insertCustomersinDB(SplashActivity.this);

            }
            List<SupervisorModel> supervisorModelList = SupervisorMgr.getInstance(SplashActivity.this).getsupervisorList(userid);
            if (supervisorModelList.isEmpty()) {
                Log.i("shravan", "supervisorModelList empty");
                SupervisorIntentService.insertSupervisorsList(SplashActivity.this);

            }


        }
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=en")
                        .timeout(6000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                    initServices();
                    Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                    startActivity(in);
                    finish();

                } else {
                    Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                    startActivity(in);
                    finish();
                }
            }
            return newVersion;
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                playstoreVersion = onlineVersion;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                            initServices();
                            if (currentVersion != null && playstoreVersion != null && isMandate != null && !currentVersion.equalsIgnoreCase(playstoreVersion) && isMandate.equalsIgnoreCase("true")) {
                                showUpdateDialog();
                            } else {
                                Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                                startActivity(in);
                                finish();
                            }
                        } else {
                            if (currentVersion != null && playstoreVersion != null && isMandate != null && !currentVersion.equalsIgnoreCase(playstoreVersion) && isMandate.equalsIgnoreCase("true")) {
                                showUpdateDialog();
                            } else {
                                Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                                startActivity(in);
                                finish();
                            }
                        }
                    }
                }, SPLASH_TIME_OUT);
            }
        }
    }

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("A New Update is Available");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceManager.getInstance().putString(Constants.BRANCHES_LAST_FETCH_TIME, "");
                SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_FETCH_TIME, "");
                SharedPreferenceManager.getInstance().putString(Constants.LEAD_LAST_FETCH_TIME, "");
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("market://details?id=com.outwork.sudlife.lite")));
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        updatedialog = builder.show();
    }

    private void getVersionHistory(String versioncode, String opSys) {
        UserRestInterface client = RestService.createServicev1(UserRestInterface.class);
        Call<RestResponse> user = client.versionHistory(versioncode, opSys);

        user.enqueue(new Callback<RestResponse>() {
            @Override
            public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                if (response.isSuccessful()) {
                    RestResponse restResponse = response.body();
                    String status = restResponse.getStatus();
                    String data = restResponse.getData();
                    if (status.equalsIgnoreCase("Success")) {
                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            isMandate = jsonObject.getString("isupgrademandatory");
                            new GetVersionCode().execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                            initServices();
                            Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                            startActivity(in);
                            finish();
                        } else {
                            Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                            startActivity(in);
                            finish();
                        }
                    }
                } else {
                    if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                        initServices();
                        Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                        startActivity(in);
                        finish();
                    } else {
                        Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                        startActivity(in);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<RestResponse> call, Throwable t) {
                if (Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, "")) && Utils.isNotNullAndNotEmpty(SharedPreferenceManager.getInstance().getString(Constants.LOGININTIME, ""))) {
                    initServices();
                    Intent in = new Intent(SplashActivity.this, CalenderActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent(SplashActivity.this, SignInActvity.class);
                    startActivity(in);
                    finish();
                }
            }
        });
    }
}

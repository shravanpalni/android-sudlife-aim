package com.outwork.sudlife.lite.localbase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.outwork.sudlife.lite.lead.model.CategoryModel;

/**
 * Created by Panch on 10/5/2015.
 */
public class ProductMasterDto {

    private int id;

    @SerializedName("category")
    @Expose
    private CategoryModel category;

    @SerializedName("productid")
    @Expose
    private String productId;
    @SerializedName("productname")
    @Expose
    private String productName;

    @SerializedName("isdeleted")
    @Expose
    private Boolean isdeleted;

    private String groupId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }


    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

}
package com.outwork.sudlife.lite.ui.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.Gson;
import com.outwork.sudlife.lite.R;
import com.outwork.sudlife.lite.lead.LeadMgr;
import com.outwork.sudlife.lite.lead.model.LeadModel;
import com.outwork.sudlife.lite.planner.PlannerMgr;
import com.outwork.sudlife.lite.planner.models.PlannerModel;
import com.outwork.sudlife.lite.planner.service.PlannerService;
import com.outwork.sudlife.lite.restinterfaces.RestResponse;
import com.outwork.sudlife.lite.restinterfaces.RestService;
import com.outwork.sudlife.lite.ui.BaseActivity;
import com.outwork.sudlife.lite.utilities.Constants;
import com.outwork.sudlife.lite.utilities.OfflineDump;
import com.outwork.sudlife.lite.utilities.SharedPreferenceManager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by shravanch on 22-10-2019.
 */

public class SyncActivity extends BaseActivity {

    private List<LeadModel> leadModelList;
    private List<PlannerModel> offlineplannerList;
    private String count = "";
    private TextView sync_plans,sync_leads,plans_count,leads_count,plans_offline_count,leads_offline_count,plans_synctext,leads_synctext;
    private CheckBox plans_checkbox,leads_checkbox;
    private boolean isSyncing = false;
    private OfflineDump offlineDump;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        leadModelList = LeadMgr.getInstance(SyncActivity.this).getOfflineLeadsListForSync(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");

        offlineplannerList = PlannerMgr.getInstance(SyncActivity.this).getOfflineplannerListForSync(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");

        initToolBar();
        initUi();



        if (isNetworkAvailable()) {
            new GetPlansSync().execute();
        } else {
            showSimpleAlert("", /*getResources().getString(R.string.no_internet_msg)*/"Oops.... No internet!");
        }




    }

    private void initToolBar() {


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText(/*getResources().getString(R.string.sync)*/"Sync");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
        }

    }


    private void initUi() {

        sync_plans = (TextView) findViewById(R.id.sync_plans);
        sync_leads = (TextView) findViewById(R.id.sync_leads);

        plans_count = (TextView) findViewById(R.id.plans_count);
        leads_count = (TextView) findViewById(R.id.leads_count);


        plans_offline_count = (TextView) findViewById(R.id.plans_offline_count);
        leads_offline_count = (TextView) findViewById(R.id.leads_offline_count);

        plans_synctext = (TextView) findViewById(R.id.plans_synctext);
        leads_synctext = (TextView) findViewById(R.id.leads_synctext);


        plans_offline_count.setText(String.valueOf(offlineplannerList.size()));
        leads_offline_count.setText(String.valueOf(leadModelList.size()));

        plans_checkbox = (CheckBox) findViewById(R.id.plans_checkbox);
        leads_checkbox = (CheckBox) findViewById(R.id.leads_checkbox);


    }





    private class GetPlansSync extends AsyncTask<Void, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isSyncing = true;
            plans_synctext.setText("Syncing. . . ");
            plans_synctext.setTextColor(getResources().getColor(R.color.daycolor));


            if (!(SyncActivity.this.isFinishing())) {

                progressDialog = new ProgressDialog(SyncActivity.this);
                progressDialog.setMessage("Syncing Plans . . . ");
                progressDialog.show();
            }

        }

        @Override

        protected String doInBackground(Void... voids) {

            String result = null;

            try {

                List<PlannerModel> plannerModelList = PlannerMgr.getInstance(SyncActivity.this).getOfflineplannerListForSync(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");

                count = "";
                if (plannerModelList.size() > 0) {


                    count = String.valueOf(plannerModelList.size());

                    for (final PlannerModel customerModel : plannerModelList) {

                        int localid = customerModel.getLid();
                        String type = "Plans";
                        Gson gson = new Gson();
                        String json = gson.toJson(customerModel);
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        long time = timestamp.getTime();

                        offlineDump = new OfflineDump();
                        offlineDump.setInputData(json);
                        offlineDump.setType(type);
                        offlineDump.setLocalId(localid);
                        offlineDump.setLocalDate(String.valueOf(time));


                        PlannerService client = RestService.createServicev1(PlannerService.class);
                        Call<RestResponse> getofflinedumps = client.syncOfflineDump(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), offlineDump);

                        try {
                            Response<RestResponse> restResponse = getofflinedumps.execute();
                            //Log.i("SyncActivity", "GetCustomerSync restResponse=====" + restResponse);
                            int statusCode = restResponse.code();
                            if (statusCode == 200) {
                                result = "success";
                                Log.i("SyncActivity", "GetPlansSync statusCode=====" + result);
                                // result = restResponse.body().getData();

                            } else {
                                result = "failure";

                                Log.i("SyncActivity", "GetPlansSync statusCode=====" + result);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;


        }


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);


        }

        @Override

        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            sync_plans.setTextColor(getResources().getColor(R.color.black_alpha_50));
            if (!count.equalsIgnoreCase("")) {
                plans_count.setText(count);
            } else {

                plans_count.setText("0");

            }

            plans_checkbox.setVisibility(View.VISIBLE);
            plans_checkbox.setChecked(true);
            plans_checkbox.setClickable(false);
            isSyncing = false;
            plans_synctext.setText("Synced");
            plans_synctext.setTextColor(getResources().getColor(R.color.main_color_grey_700));

           /* // getting all records with null or empty customerid's from contact table
            List<LeadModel> leadsList = new ArrayList<>();
            leadsList = LeadMgr.getInstance(SyncActivity.this).getOfflineLeadsListForSync(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
            if (leadsList.size() > 0) {

                for (final LeadModel leadsModel : leadsList) {

                    // getting customer localid from above list
                    int custlocalid = contactModel.getCustomerlid();

                    // getting custid with the help of clid from customer table
                    String custid = ContactMgr.getInstance(SyncActivity.this).getCustomerIdUsingLocalid(custlocalid);
                    //updating customerid from above result to customerid in contact table
                    boolean a = ContactMgr.getInstance(SyncActivity.this).updateCustidinContactTable(custid);


                }


            }*/
            if (isNetworkAvailable()) {

                new GetLeadsSync().execute();

            } else {
                showSimpleAlert("", "Oops! No internet!");
            }


        }
    }






    private class GetLeadsSync extends AsyncTask<Void, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isSyncing = true;
            leads_synctext.setText("Syncing. . . ");
            leads_synctext.setTextColor(getResources().getColor(R.color.daycolor));


            if (!(SyncActivity.this.isFinishing())) {

                progressDialog = new ProgressDialog(SyncActivity.this);
                progressDialog.setMessage("Syncing Leads . . . ");
                progressDialog.show();
            }

        }

        @Override

        protected String doInBackground(Void... voids) {

            String result = null;

            try {

                List<LeadModel> leadsList = LeadMgr.getInstance(SyncActivity.this).getOfflineLeadsListForSync(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");

                count = "";
                if (leadsList.size() > 0) {


                    count = String.valueOf(leadsList.size());

                    for (final LeadModel leadsModel : leadsList) {

                        int localid = leadsModel.getId();
                        String type = "Leads";
                        Gson gson = new Gson();
                        String json = gson.toJson(leadsModel);
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        long time = timestamp.getTime();

                        offlineDump = new OfflineDump();
                        offlineDump.setInputData(json);
                        offlineDump.setType(type);
                        offlineDump.setLocalId(localid);
                        offlineDump.setLocalDate(String.valueOf(time));


                        PlannerService client = RestService.createServicev1(PlannerService.class);
                        Call<RestResponse> getofflinedumps = client.syncOfflineDump(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), offlineDump);

                        try {
                            Response<RestResponse> restResponse = getofflinedumps.execute();
                            //Log.i("SyncActivity", "GetCustomerSync restResponse=====" + restResponse);
                            int statusCode = restResponse.code();
                            if (statusCode == 200) {
                                result = "success";
                                Log.i("SyncActivity", "GetLeadsSync statusCode=====" + result);
                                // result = restResponse.body().getData();

                            } else {
                                result = "failure";

                                Log.i("SyncActivity", "GetLeadsSync statusCode=====" + result);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;


        }


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);


        }

        @Override

        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            sync_leads.setTextColor(getResources().getColor(R.color.black_alpha_50));
            if (!count.equalsIgnoreCase("")) {
                leads_count.setText(count);
            } else {

                leads_count.setText("0");

            }

            leads_checkbox.setVisibility(View.VISIBLE);
            leads_checkbox.setChecked(true);
            leads_checkbox.setClickable(false);
            isSyncing = false;
            leads_synctext.setText("Synced");
            leads_synctext.setTextColor(getResources().getColor(R.color.main_color_grey_700));



        }
    }


}

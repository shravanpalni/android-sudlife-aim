package com.outwork.sudlife.lite.planner.service;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.lite.branches.activities.ListCustomerActivity;
import com.outwork.sudlife.lite.branches.services.ContactsIntentService;
import com.outwork.sudlife.lite.lead.services.LeadIntentService;
import com.outwork.sudlife.lite.planner.PlannerMgr;
import com.outwork.sudlife.lite.planner.models.PlannerModel;
import com.outwork.sudlife.lite.restinterfaces.RestResponse;
import com.outwork.sudlife.lite.restinterfaces.RestService;
import com.outwork.sudlife.lite.targets.services.TargetsIntentService;
import com.outwork.sudlife.lite.utilities.Constants;
import com.outwork.sudlife.lite.utilities.SharedPreferenceManager;
import com.outwork.sudlife.lite.utilities.TimeUtils;
import com.outwork.sudlife.lite.utilities.Utils;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class PlannerIntentService extends JobIntentService {
    public static final String TAG = PlannerIntentService.class.getSimpleName();

    public static final String ACTION_GET_PLAN = "com.outwork.sudlife.lite.planner.service.action.GET_PLAN";
    public static final String ACTION_POST_PLAN = "com.outwork.sudlife.lite.planner.service.action.POST_PLAN";
    public static final String ACTION_PLAN_OFFLINE_SYNC = "com.outwork.sudlife.lite.planner.service.action.TOUR_OFFLINE_SYNC";
    public static final String ACTION_UPDATE_PLAN = "com.outwork.sudlife.lite.planner.service.action.UPDATE_PLAN";

    public static final String PLAN_MODEL = "com.outwork.sudlife.lite.planner.service.extra.PLAN_MODEL";
    public static final String MONTH = "com.outwork.sudlife.lite.planner.service.extra.MONTH";
    public static final String YEAR = "com.outwork.sudlife.lite.planner.service.extra.YEAR";
    private static final Integer JOBID = 1004;
    private LocalBroadcastManager mgr;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_PLAN.equals(action)) {
                final String month = intent.getStringExtra(MONTH);
                final String year = intent.getStringExtra(YEAR);
                getServerPlannerList(month, year);
            }
            if (ACTION_POST_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                postPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_UPDATE_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                updatePlannedPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_PLAN_OFFLINE_SYNC.equals(action)) {
                syncOfflinePlans();
            }
        }
    }

    public static void insertPlanList(Context context, String month, String year) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_GET_PLAN);
        intent.putExtra(MONTH, month);
        intent.putExtra(YEAR, year);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }

    public static void syncPlanstoServer(Context context) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_PLAN_OFFLINE_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class, JOBID, intent);
        } else {
            context.startService(intent);
        }
    }


   /* public static String convertSecondsToHMmSs(long millis) {
        //long seconds = (millis / 1000) % 60;
        long minutes = (millis / (1000 * 60)) % 60;
        long hours = millis / (1000 * 60 * 60);

        StringBuilder b = new StringBuilder();
        b.append(hours == 0 ? "00" : hours < 10 ? String.valueOf("0" + hours) :
                String.valueOf(hours));
        b.append(":");
        b.append(minutes == 0 ? "00" : minutes < 10 ? String.valueOf("0" + minutes) :
                String.valueOf(minutes));
       *//* b.append(":");
        b.append(seconds == 0 ? "00" : seconds < 10 ? String.valueOf("0" + seconds) :
                String.valueOf(seconds));*//*
        return b.toString();
    }*/

    private void syncOfflinePlans() {


        String storedTime = SharedPreferenceManager.getInstance().getString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
        Log.i("Shravan==storedTime===", storedTime);


        if (storedTime.equalsIgnoreCase("") || storedTime == null) {
            final List<PlannerModel> offlineplannerList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
            if (offlineplannerList.size() > 0) {
                for (final PlannerModel plannerModel : offlineplannerList) {
                    if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
                        if (isNetworkAvailable()) {
                            if (!Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                                postPlan(plannerModel);
                            } else {
                                if (plannerModel.getCompletestatus() == 0) {
                                    updatePlannedPlan(plannerModel);
                                } else if (plannerModel.getCompletestatus() == 1) {
                                    updateCheckin(plannerModel);
                                } else if (plannerModel.getCompletestatus() == 2) {
                                    updateCheckout(plannerModel);
                                } else if (plannerModel.getCompletestatus() == 3) {
                                    updateReschedule(plannerModel);
                                }
                            }
                        }
                    }
                }
            }

        } else {
            long currentTime = System.currentTimeMillis();
            Log.i("Shravan==time===", String.valueOf(currentTime));
            long previoustime = Long.parseLong(storedTime);
            long latesttime = currentTime;
            long diffrence = latesttime - previoustime;
          /*  String myValue = convertSecondsToHMmSs(diffrence);
            Log.i("Shravan==myValue===", myValue);*/
            //DateTime startTime, endTime;
            Period p = new Period(previoustime, latesttime);
            long hours = p.getHours();
            long minutes = p.getMinutes();
            Log.i("Shravan==hours===", String.valueOf(hours));
            Log.i("Shravan==minutes===", String.valueOf(minutes));
            if ((Integer.parseInt(String.valueOf(hours)) >= 0) && (Integer.parseInt(String.valueOf(minutes)) > 30)) {
                Log.i("shravan", "storedtime >30 minutes");
                final List<PlannerModel> offlineplannerList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
                if (offlineplannerList.size() > 0) {
                    for (final PlannerModel plannerModel : offlineplannerList) {
                        if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
                            if (isNetworkAvailable()) {
                                if (!Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                                    postPlan(plannerModel);
                                } else {
                                    if (plannerModel.getCompletestatus() == 0) {
                                        updatePlannedPlan(plannerModel);
                                    } else if (plannerModel.getCompletestatus() == 1) {
                                        updateCheckin(plannerModel);
                                    } else if (plannerModel.getCompletestatus() == 2) {
                                        updateCheckout(plannerModel);
                                    } else if (plannerModel.getCompletestatus() == 3) {
                                        updateReschedule(plannerModel);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    private void updateCheckout(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkout(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus())) {
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "2");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                    TargetsIntentService.insertTargetRecords(PlannerIntentService.this);

                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }
    }

    private void postPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> postPlan = client.postPlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = postPlan.execute();
            int statusCode = restResponse.code();
            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus())) {
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        } else {
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerdummyOnline(String.valueOf(plannerModel.getLid()), userid);
                        }
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }

                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }
    }

    private void updateReschedule(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.reschdule(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus())) {
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "3");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }
    }

    private void updateCheckin(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkin(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus())) {
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "1");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }
    }

    private void updatePlannedPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updatePlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus())) {
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, "");
            SharedPreferenceManager.getInstance().putString(Constants.LAST_SYNC_FETCH_TIME_PLAN, String.valueOf(System.currentTimeMillis()));
            e.printStackTrace();
        }
    }

    private void updateApproval(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updateApprove(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), String.valueOf(plannerModel.getPlanstatus()));
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            if (Utils.isNotNullAndNotEmpty(restResponse.body().toString())) {
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                        if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                            Intent intent = new Intent("plan_broadcast");
                            mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerPlannerList(final String month, String year) {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.PLANNER_LAST_FETCH_TIME, "");
        List<PlannerModel> plannerModelList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "");
        if (plannerModelList.size() == 0) {
            lastfetchtime = "";
        }
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getplans = client.getPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), month, year, lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        try {
            Response<RestResponse> restResponse = getplans.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (!TextUtils.isEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PlannerModel>>() {
                        }.getType();
                        List<PlannerModel> plannerModelList1 = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (plannerModelList1.size() > 0) {
                            PlannerMgr.getInstance(PlannerIntentService.this).insertPlannerList(plannerModelList1, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                        }

                        if (SharedPreferenceManager.getInstance().getString(Constants.LEADS_LOADED, "").equals("notloaded")) {
                            if (isNetworkAvailable()) {
                                LeadIntentService.insertLeadstoServer(PlannerIntentService.this);
                            }
                        }

                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "loaded");
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_LOADED_TIME, String.valueOf(System.currentTimeMillis()));
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_FETCH_TIME, newFetchTime);
                        TargetsIntentService.insertTargetRecords(PlannerIntentService.this);
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }
}









/*
package com.outwork.sudlife.lite.planner.service;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outwork.sudlife.lite.lead.services.LeadIntentService;
import com.outwork.sudlife.lite.planner.PlannerMgr;
import com.outwork.sudlife.lite.planner.models.PlannerModel;
import com.outwork.sudlife.lite.restinterfaces.RestResponse;
import com.outwork.sudlife.lite.restinterfaces.RestService;
import com.outwork.sudlife.lite.targets.services.TargetsIntentService;
import com.outwork.sudlife.lite.utilities.Constants;
import com.outwork.sudlife.lite.utilities.SharedPreferenceManager;
import com.outwork.sudlife.lite.utilities.TimeUtils;
import com.outwork.sudlife.lite.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class PlannerIntentService extends JobIntentService {
    public static final String TAG = PlannerIntentService.class.getSimpleName();

    public static final String ACTION_GET_PLAN = "com.outwork.sudlife.lite.planner.service.action.GET_PLAN";
    public static final String ACTION_POST_PLAN = "com.outwork.sudlife.lite.planner.service.action.POST_PLAN";
    public static final String ACTION_PLAN_OFFLINE_SYNC = "com.outwork.sudlife.lite.planner.service.action.TOUR_OFFLINE_SYNC";
    public static final String ACTION_UPDATE_PLAN = "com.outwork.sudlife.lite.planner.service.action.UPDATE_PLAN";

    public static final String PLAN_MODEL = "com.outwork.sudlife.lite.planner.service.extra.PLAN_MODEL";
    public static final String MONTH = "com.outwork.sudlife.lite.planner.service.extra.MONTH";
    public static final String YEAR = "com.outwork.sudlife.lite.planner.service.extra.YEAR";
    private static final Integer JOBID = 1004;
    private LocalBroadcastManager mgr;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_PLAN.equals(action)) {
                final String month = intent.getStringExtra(MONTH);
                final String year = intent.getStringExtra(YEAR);
                getServerPlannerList(month, year);
            }
            if (ACTION_POST_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                postPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_UPDATE_PLAN.equals(action)) {
                final String plannerModel = intent.getStringExtra(PLAN_MODEL);
                updatePlannedPlan(new Gson().fromJson(plannerModel, PlannerModel.class));
            }
            if (ACTION_PLAN_OFFLINE_SYNC.equals(action)) {
                syncOfflinePlans();
            }
        }
    }

    public static void insertPlanList(Context context, String month, String year) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_GET_PLAN);
        intent.putExtra(MONTH, month);
        intent.putExtra(YEAR, year);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    public static void syncPlanstoServer(Context context) {
        Intent intent = new Intent(context, PlannerIntentService.class);
        intent.setAction(ACTION_PLAN_OFFLINE_SYNC);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enqueueWork(context, PlannerIntentService.class,JOBID,intent);
        } else {
            context.startService(intent);
        }
    }

    private void syncOfflinePlans() {
        final List<PlannerModel> offlineplannerList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "offline");
        if (offlineplannerList.size() > 0) {
            for (final PlannerModel plannerModel : offlineplannerList) {
                if (Utils.isNotNullAndNotEmpty(plannerModel.getNetwork_status())) {
                    if (isNetworkAvailable()) {
                        if (!Utils.isNotNullAndNotEmpty(plannerModel.getActivityid())) {
                            postPlan(plannerModel);
                        } else {
                            if (plannerModel.getCompletestatus() == 0) {
                                updatePlannedPlan(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 1) {
                                updateCheckin(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 2) {
                                updateCheckout(plannerModel);
                            } else if (plannerModel.getCompletestatus() == 3) {
                                updateReschedule(plannerModel);
                            }
                        }
                    }
                }
            }
        }
    }

    private void postPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> postPlan = client.postPlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = postPlan.execute();
            int statusCode = restResponse.code();
            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        if (Utils.isNotNullAndNotEmpty(restResponse.body().getData())) {
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        } else {
                            PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerdummyOnline(String.valueOf(plannerModel.getLid()), userid);
                        }
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCheckout(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkout(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "2");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                TargetsIntentService.insertTargetRecords(PlannerIntentService.this);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateReschedule(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.reschdule(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "3");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCheckin(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.checkin(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerStatus(String.valueOf(plannerModel.getLid()), userid, "1");
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updatePlannedPlan(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updatePlan(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel);
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                    if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                        String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                        PlannerMgr.getInstance(PlannerIntentService.this).updatePlannerID(String.valueOf(plannerModel.getLid()), userid, restResponse.body().getData());
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateApproval(final PlannerModel plannerModel) {
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> updatePlan = client.updateApprove(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), plannerModel.getActivityid(), String.valueOf(plannerModel.getPlanstatus()));
        try {
            Response<RestResponse> restResponse = updatePlan.execute();
            if (Utils.isNotNullAndNotEmpty(restResponse.body().toString())) {
                int statusCode = restResponse.code();
                if (statusCode == 200) {
                    if (Utils.isNotNullAndNotEmpty(restResponse.body().getStatus()))
                        if (restResponse.body().getStatus().equalsIgnoreCase(Constants.success)) {
                            String userid = SharedPreferenceManager.getInstance().getString(Constants.USERID, "");
                            Intent intent = new Intent("plan_broadcast");
                            mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                            mgr.sendBroadcast(intent);
                        }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getServerPlannerList(final String month, String year) {
        String lastfetchtime = SharedPreferenceManager.getInstance().getString(Constants.PLANNER_LAST_FETCH_TIME, "");
        List<PlannerModel> plannerModelList = PlannerMgr.getInstance(PlannerIntentService.this).getOfflineplannerList(SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), "");
        if (plannerModelList.size() == 0) {
            lastfetchtime = "";
        }
        PlannerService client = RestService.createServicev1(PlannerService.class);
        Call<RestResponse> getplans = client.getPlans(SharedPreferenceManager.getInstance().getString(Constants.USER_TOKEN, ""), month, year, lastfetchtime);
        final String newFetchTime = TimeUtils.getCurrentUnixTimeStamp();
        try {
            Response<RestResponse> restResponse = getplans.execute();
            int statusCode = restResponse.code();
            if (statusCode == 200) {
                try {
                    if (!TextUtils.isEmpty(restResponse.body().getData())) {
                        Type listType = new TypeToken<ArrayList<PlannerModel>>() {
                        }.getType();
                        List<PlannerModel> plannerModelList1 = new Gson().fromJson(restResponse.body().getData(), listType);
                        if (plannerModelList1.size() > 0) {
                            PlannerMgr.getInstance(PlannerIntentService.this).insertPlannerList(plannerModelList1, SharedPreferenceManager.getInstance().getString(Constants.USERID, ""), SharedPreferenceManager.getInstance().getString(Constants.GROUPID, ""));
                        }
                        LeadIntentService.insertLeadstoServer(PlannerIntentService.this);
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "loaded");
                        SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LAST_FETCH_TIME, newFetchTime);
                        TargetsIntentService.insertTargetRecords(PlannerIntentService.this);
                        Intent intent = new Intent("plan_broadcast");
                        mgr = LocalBroadcastManager.getInstance(PlannerIntentService.this);
                        mgr.sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
                }
            }
        } catch (IOException e) {
            SharedPreferenceManager.getInstance().putString(Constants.PLANNER_LOADED, "notloaded");
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }
}*/
